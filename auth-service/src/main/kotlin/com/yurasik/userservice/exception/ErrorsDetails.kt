package com.yurasik.userservice.exception

import java.util.*

/*{
    "timestamp": "2019-10-07T14:52:54.987+0000",
    "status": 403,
    "error": "Forbidden",
    "message": "Access Denied",
    "path": "/api/private"
}*/

data class ErrorsDetails(val timestamp: Date? =Date(),
                         val status: Int,
                         val error: String? =null,
                         val message: String? =null,
                         val path: String? = "")