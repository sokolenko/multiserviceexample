package com.yurasik.userservice.exception

import org.slf4j.LoggerFactory
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes
import org.springframework.boot.web.servlet.error.ErrorAttributes
import org.springframework.context.annotation.Bean
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.AccessDeniedException
import org.springframework.web.bind.annotation.*
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.NoHandlerFoundException
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

import javax.servlet.http.HttpServletResponse
import java.io.IOException
import java.security.SignatureException
import java.util.*

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
//@RequestMapping(produces = ["application/json"])
class GlobalExceptionHandlerController : ResponseEntityExceptionHandler() {

    companion object {
        val log = LoggerFactory.getLogger(GlobalExceptionHandlerController::class.java)
    }

    @Bean
    fun errorAttributes(): ErrorAttributes {
        // Hide exception field in the return object
        return object : DefaultErrorAttributes() {

            override fun getErrorAttributes(webRequest: WebRequest, includeStackTrace: Boolean): Map<String, Any> {

                val errorAttributes = super.getErrorAttributes(webRequest, includeStackTrace)
                log.info("Error attributes: $errorAttributes")
                errorAttributes.remove("exception")
                return errorAttributes
            }

        }
    }

    @ExceptionHandler(CustomException::class)
    @Throws(IOException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun handleCustomException(res: HttpServletResponse, ex: CustomException):  ResponseEntity<ErrorsDetails> {
        log.info("handleCustomException: $ex")
        res.sendError(ex.httpStatus.value(), ex.message)
        val errorDetails = ErrorsDetails(status = HttpStatus.FORBIDDEN.value(),
            error = ex::class.java.simpleName,
            message = ex.message
        )
        return ResponseEntity(errorDetails, HttpStatus.BAD_REQUEST)
    }

    @ExceptionHandler(SignatureException::class)
    @Throws(IOException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun handleSignatureException(res: HttpServletResponse, ex: SignatureException):  ResponseEntity<ErrorsDetails> {
        log.info("handleSignatureException: $ex")
        res.sendError(HttpStatus.FORBIDDEN.value(), ex.message)
        val errorDetails = ErrorsDetails(status = HttpStatus.FORBIDDEN.value(),
            error = ex::class.java.simpleName,
            message = ex.message
        )
        return ResponseEntity(errorDetails, HttpStatus.BAD_REQUEST)
    }

    override fun handleNoHandlerFoundException(
        ex: NoHandlerFoundException,
        headers: HttpHeaders,
        status: HttpStatus,
        request: WebRequest
    ): ResponseEntity<Any> {
        logger.info("handleNoHandlerFoundException")
        return super.handleNoHandlerFoundException(ex, headers, status, request)
    }

    /*@ExceptionHandler()
    @Throws(IOException::class)
    fun handleAccessDeniedException(res: HttpServletResponse): ErrorsDetails {
        log.info("handleAccessDeniedException: $res")
        res.sendError(HttpStatus.FORBIDDEN.value(), "Access denied")
        return ErrorsDetails(430, "Access denied")
    }*/

    /*@ExceptionHandler(value = [(AccessDeniedException::class)])
    fun handleUserAlreadyExists(ex: AccessDeniedException,request: WebRequest): ResponseEntity<ErrorsDetails> {
        log.info("handleAccessDeniedException: $ex")
        val errorDetails = ErrorsDetails(
            Date(),
            "Validation Failed",
            ex.message!!
        )
        return ResponseEntity(errorDetails, HttpStatus.BAD_REQUEST)
    }*/

    @ExceptionHandler(Exception::class)
    @Throws(IOException::class)
    fun handleException(res: HttpServletResponse) {
        log.info("handleException: $res")
        res.sendError(HttpStatus.BAD_REQUEST.value(), "Something went wrong")
    }

}
