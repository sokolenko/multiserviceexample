package com.yurasik.userservice.exception

import org.springframework.http.HttpStatus

class CustomException(override val message: String? =null,
                      val httpStatus: HttpStatus,
                      val parentException: Exception? = null) : Exception()