package com.yurasik.userservice.security.model

import org.springframework.security.core.GrantedAuthority

enum class RoleEnum : GrantedAuthority {
    ANONYMOUS, USER, ADMIN;

    override fun getAuthority() = name

}