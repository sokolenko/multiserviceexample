package com.yurasik.userservice.security.jwt

import com.yurasik.userservice.exception.CustomException
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.filter.OncePerRequestFilter
import java.io.IOException
import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import org.apache.catalina.manager.StatusTransformer.setContentType
import org.springframework.http.MediaType
import com.fasterxml.jackson.databind.ObjectMapper
import com.yurasik.userservice.exception.ErrorsDetails
import org.springframework.beans.factory.annotation.Autowired
import java.util.*

// We should use OncePerRequestFilter since we are doing a database call, there is no point in doing this more than once
class JwtTokenFilter(private val jwtTokenProvider: JwtTokenProvider) : OncePerRequestFilter() {

    companion object {
        val log = LoggerFactory.getLogger(JwtTokenFilter::class.java)
    }

    private val mapper = ObjectMapper()

    @Throws(ServletException::class, IOException::class, CustomException::class)
    override fun doFilterInternal(
        httpServletRequest: HttpServletRequest,
        httpServletResponse: HttpServletResponse,
        filterChain: FilterChain
    ) {
        val token = jwtTokenProvider.resolveToken(httpServletRequest)
        log.info("doFilterInternal : $token")

        try {
            if (token != null && jwtTokenProvider.validateToken(token)) {
                val auth = jwtTokenProvider.getAuthentication(token)
                SecurityContextHolder.getContext().authentication = auth
            }
            filterChain.doFilter(httpServletRequest, httpServletResponse)
        } catch (ex: Exception) {
            //this is very important, since it guarantees the user is not authenticated at all
//            placeErrorMessage(ex, httpServletResponse, httpServletRequest)
            ex.printStackTrace()
            httpServletResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.message)
        }
    }

    private fun placeErrorMessage(
        ex: CustomException,
        httpServletResponse: HttpServletResponse,
        httpServletRequest: HttpServletRequest
    ) {
        log.info("Exception: ${ex.message}")
        SecurityContextHolder.clearContext()
        // notify client of response body content type
        httpServletResponse.addHeader("Content-Type", "application/json;charset=UTF-8");
        httpServletResponse.status = HttpServletResponse.SC_UNAUTHORIZED
        val detail = ErrorsDetails(
            timestamp = Date(),
            status = HttpServletResponse.SC_UNAUTHORIZED,
            error = ex.parentException?.javaClass?.simpleName ?: ex.message,
            message = ex.parentException?.message ?: ex.message,
            path = httpServletRequest.requestURL.toString() + "/" + (httpServletRequest.queryString ?: "")
        )
        mapper.writeValue(httpServletResponse.outputStream, detail)

        httpServletResponse.flushBuffer()
    }

}