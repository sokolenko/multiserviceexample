package com.yurasik.userservice.security.model

data class LoginData(val username: String, val password: String)