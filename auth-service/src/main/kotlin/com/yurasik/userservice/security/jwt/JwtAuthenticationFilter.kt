package com.yurasik.userservice.security.jwt

import com.yurasik.userservice.security.SecurityConstants
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.userdetails.User
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter

import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import com.fasterxml.jackson.databind.ObjectMapper
import com.yurasik.userservice.exception.CustomException
import com.yurasik.userservice.security.model.LoginData
import com.yurasik.userservice.security.model.TokensResponse
import javassist.tools.web.BadHttpRequest
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import java.io.IOException
import javax.management.BadAttributeValueExpException

class JwtAuthenticationFilter(
    private val authenticationManagerBean: AuthenticationManager,
    private val jwtTokenProvider: JwtTokenProvider,
    private val mapper: ObjectMapper
) : UsernamePasswordAuthenticationFilter() {

    companion object {
        val log = LoggerFactory.getLogger(JwtAuthenticationFilter::class.java)
    }

    init {
        setFilterProcessesUrl(SecurityConstants.AUTH_LOGIN_URL)
        authenticationManager = authenticationManagerBean
        log.info("authenticationManager: $authenticationManagerBean")
    }

    override fun attemptAuthentication(request: HttpServletRequest, response: HttpServletResponse?): Authentication {
        val username = request.getParameter("username")
        val password = request.getParameter("password")
        val authenticationToken = (if (username == null || password == null) {
            parseLoginData(request)
        } else {
            UsernamePasswordAuthenticationToken(username, password)
        })
            ?: throw CustomException(parentException = BadAttributeValueExpException("Bad arguments"),
                httpStatus = HttpStatus.BAD_REQUEST, message = "No arguments")
        log.info("AuthToken: $authenticationToken")
        return authenticationManager.authenticate(authenticationToken)
    }

    override fun successfulAuthentication(
        request: HttpServletRequest, response: HttpServletResponse,
        filterChain: FilterChain?, authentication: Authentication
    ) {
        val user = authentication.principal as User
        val token = jwtTokenProvider.createAccessJwtToken(user)
        val refreshToken = jwtTokenProvider.createRefreshToken(user.username)
        response.addHeader("Content-Type", "application/json;charset=UTF-8");
        response.addHeader(SecurityConstants.TOKEN_HEADER, SecurityConstants.TOKEN_PREFIX + token)
        val detail = TokensResponse(
            status = HttpServletResponse.SC_OK,
            token = token,
            refreshToken = refreshToken,
            message = "Token created",
            path = request.requestURL.toString() + "/" + (request.queryString ?: "")
        )
        mapper.writeValue(response.outputStream, detail)
    }

    private fun parseLoginData(request: HttpServletRequest): UsernamePasswordAuthenticationToken? {
        return try {
            val mapper = ObjectMapper()
            val loginData = mapper.readValue(request.inputStream, LoginData::class.java)
            UsernamePasswordAuthenticationToken(loginData.username, loginData.password)
        } catch (exception: IOException) {
            // Return empty "invalid" login data
            null
        }
    }
}
