package com.yurasik.userservice.security.jwt.processing

import org.springframework.security.core.Authentication
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter
import org.springframework.security.web.util.matcher.RequestMatcher
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import com.yurasik.userservice.security.configuration.WebSecurityConfig
import com.yurasik.userservice.security.jwt.JwtTokenProvider
import org.slf4j.LoggerFactory
import org.springframework.security.core.AuthenticationException
import javax.servlet.FilterChain


class JwtTokenAuthenticationProcessingFilter(val requiresAuthenticationRequestMatcher: RequestMatcher,
                                             val tokenExtractor: TokenExtractor,
                                             val matcher: RequestMatcher) :
    AbstractAuthenticationProcessingFilter(requiresAuthenticationRequestMatcher) {

    companion object {
        val log = LoggerFactory.getLogger(AbstractAuthenticationProcessingFilter::class.java)
    }

    override fun attemptAuthentication(request: HttpServletRequest?, response: HttpServletResponse?): Authentication {
        //val tokenPayload = request?.getHeader("token")
        //val token = RawAccessJwtToken(tokenExtractor.extract(tokenPayload))
        log.info("attemptAuthentication: $request")
        return authenticationManager.authenticate(null)
    }

    override fun successfulAuthentication(
        request: HttpServletRequest?,
        response: HttpServletResponse?,
        chain: FilterChain?,
        authResult: Authentication?
    ) {
        super.successfulAuthentication(request, response, chain, authResult)

        log.info("successfulAuthentication: $request")
    }

    override fun unsuccessfulAuthentication(
        request: HttpServletRequest?,
        response: HttpServletResponse?,
        failed: AuthenticationException?
    ) {
        super.unsuccessfulAuthentication(request, response, failed)
        log.info("unsuccessfulAuthentication: $request")
    }
}