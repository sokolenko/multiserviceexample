package com.yurasik.userservice.security.service

import com.yurasik.userservice.exception.CustomException
import com.yurasik.userservice.repository.UserRepository
import com.yurasik.userservice.security.MyUserDetails
import com.yurasik.userservice.security.jwt.JwtTokenProvider
import com.yurasik.userservice.security.model.User
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.AuthenticationException
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service

import javax.servlet.http.HttpServletRequest

@Service
class UserService {

    @Autowired
    private lateinit var userRepository: UserRepository

    @Autowired
    private lateinit var passwordEncoder: PasswordEncoder

    @Autowired
    private lateinit var jwtTokenProvider: JwtTokenProvider

    @Autowired
    private lateinit var authenticationManager: AuthenticationManager

    companion object {
        val loggger = LoggerFactory.getLogger(UserService::class.java)
    }

    /*fun signin(username: String, password: String): String {
        try {
            authenticationManager!!.authenticate(UsernamePasswordAuthenticationToken(username, password))
            return jwtTokenProvider!!.createToken(username, userRepository!!.findByUsername(username)?.roles!!)
        } catch (e: AuthenticationException) {
            throw CustomException("Invalid username/password supplied", HttpStatus.UNPROCESSABLE_ENTITY)
        }

    }*/

    fun signup(user: User): String {
        if (!userRepository.existsByUsername(user.username!!)) {
            user.password = passwordEncoder.encode(user.password)
            val u = userRepository.save(user)
            loggger.info("User: $u")
            return ""//jwtTokenProvider.createToken(user.username!!, user.roles!!)
        } else {
            throw CustomException("Username is already in use", HttpStatus.UNPROCESSABLE_ENTITY)
        }
    }

    fun delete(username: String) {
        userRepository!!.deleteByUsername(username)
    }

    fun search(username: String): User {
        return userRepository!!.findByUsername(username)
                ?: throw CustomException("The user doesn't exist", HttpStatus.NOT_FOUND)
    }

    /*fun whoami(req: HttpServletRequest): User {
        return userRepository!!.findByUsername(jwtTokenProvider!!.getUsername(jwtTokenProvider.resolveToken(req)!!))
    }

    fun refresh(username: String): String {
        return jwtTokenProvider!!.createToken(username, userRepository!!.findByUsername(username).roles!!)
    }*/

}
