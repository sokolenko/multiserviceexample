package com.yurasik.userservice.security.model

import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.annotations.BatchSize
import javax.persistence.*
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

@Entity
@Table(name = "USER")
data class User(

        @JsonIgnore
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "ID", unique = true, nullable = false)
        var id: Long? = 1,

        @Column(name = "USERNAME", length = 50, unique = true)
        @NotNull
        @Size(min = 4, max = 50)
        var username: String? = null,

        @JsonIgnore
        @Column(name = "PASSWORD", length = 100)
        @NotNull
        @Size(min = 4, max = 100)
        var password: String? = null,

        @Column(name = "FIRSTNAME", length = 50)
        @NotNull
        @Size(min = 4, max = 50)
        var firstname: String? = null,

        @Column(name = "LASTNAME", length = 50)
        @NotNull
        @Size(min = 4, max = 50)
        var lastname: String? = null,

        @Column(name = "EMAIL", length = 50)
        @NotNull
        @Size(min = 4, max = 50)
        var email: String? = null,

        @JsonIgnore
        @Column(name = "ACTIVATED")
        @NotNull
        var activated: Boolean = false,

        @BatchSize(size = 20)
        @ElementCollection(fetch = FetchType.EAGER)
        @CollectionTable(name = "RoleEnum", joinColumns = [JoinColumn(name = "ID")])
        @Enumerated(EnumType.STRING)
        @Column(name = "USER_ROLES")
        var roles: MutableSet<RoleEnum>? = mutableSetOf()
) {
    override fun toString(): String {
        return "User{" +
                "username='" + username + '\''.toString() +
                ", password='" + password + '\''.toString() +
                ", firstname='" + firstname + '\''.toString() +
                ", lastname='" + lastname + '\''.toString() +
                ", email='" + email + '\''.toString() +
                ", activated=" + activated +
                '}'.toString()
    }
}