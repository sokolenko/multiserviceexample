package com.yurasik.userservice.security.configuration

import com.fasterxml.jackson.databind.ObjectMapper
import com.yurasik.userservice.exception.GlobalExceptionHandlerController
import com.yurasik.userservice.security.jwt.JwtAccessDeniedHandler
import com.yurasik.userservice.security.jwt.JwtAuthenticationEntryPoint
import com.yurasik.userservice.security.jwt.JwtTokenFilterConfigurer
import com.yurasik.userservice.security.jwt.JwtTokenProvider
import com.yurasik.userservice.security.jwt.processing.JwtTokenAuthenticationProcessingFilter
import com.yurasik.userservice.security.jwt.processing.SkipPathRequestMatcher
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.CorsConfigurationSource
import org.springframework.web.cors.UrlBasedCorsConfigurationSource
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.BeanIds
import org.springframework.security.config.annotation.web.builders.WebSecurity
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter

@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
class WebSecurityConfig(internal val jwtTokenProvider: JwtTokenProvider) : WebSecurityConfigurerAdapter() {

    val JWT_TOKEN_HEADER_PARAM = "X-Authorization"
    val FORM_BASED_LOGIN_ENTRY_POINT = "/api/auth/login"
    val TOKEN_BASED_AUTH_ENTRY_POINT = "/api/**"
    val TOKEN_REFRESH_ENTRY_POINT = "/api/auth/token"

    @Bean(name = [BeanIds.AUTHENTICATION_MANAGER])
    @Throws(Exception::class)
    override fun authenticationManagerBean(): AuthenticationManager {
        return super.authenticationManagerBean()
    }

    @Autowired
    private lateinit var mapper: ObjectMapper

    @Autowired
    private lateinit var jwtAccessDeniedHandler: JwtAccessDeniedHandler

    @Autowired
    private lateinit var jwtAuthenticationEntryPoint: JwtAuthenticationEntryPoint

    @Autowired
    private lateinit var globalExceptionHandlerController: GlobalExceptionHandlerController

    @Autowired
    private lateinit var authenticationManager: AuthenticationManager

    @Bean
    fun buildJwtTokenAuthenticationProcessingFilter(): JwtTokenAuthenticationProcessingFilter {
        val pathToSkip = listOf<String>(TOKEN_REFRESH_ENTRY_POINT, FORM_BASED_LOGIN_ENTRY_POINT)
        val matcher = SkipPathRequestMatcher(pathToSkip, TOKEN_BASED_AUTH_ENTRY_POINT)
        val filter = JwtTokenAuthenticationProcessingFilter(matcher)
        filter.setAuthenticationManager(authenticationManager)
        return filter
    }

    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {
        http.cors().and()
            .csrf().disable()
            .authorizeRequests()
            .antMatchers("/api/public").permitAll()
            .anyRequest().authenticated()
            .and()
            //.addFilter(JwtAuthenticationFilter(authenticationManager(), jwtTokenUtil))
            //.addFilter(JwtAuthorizationFilter(authenticationManager()))
            .sessionManagement()
            .sessionCreationPolicy(SessionCreationPolicy.STATELESS)

        http.addFilterBefore(buildJwtTokenAuthenticationProcessingFilter(), UsernamePasswordAuthenticationFilter::class.java)

        //http.apply(JwtTokenFilterConfigurer(jwtTokenProvider, authenticationManagerBean(), mapper))

        //http.authenticationProvider(JwtTokenAuthenticationProcessingFilter())

        //http.exceptionHandling().accessDeniedHandler(jwtAccessDeniedHandler)
        //http.exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint)
    }

    @Throws(Exception::class)
    public override fun configure(auth: AuthenticationManagerBuilder) {

    }

    @Bean
    fun passwordEncoder(): PasswordEncoder {
        return BCryptPasswordEncoder()
    }

    @Bean
    fun corsConfigurationSource(): CorsConfigurationSource {
        val source = UrlBasedCorsConfigurationSource()
        source.registerCorsConfiguration("/**", CorsConfiguration().applyPermitDefaultValues())
        return source
    }
}
