package com.yurasik.userservice.security.jwt

import com.yurasik.userservice.exception.CustomException
import com.yurasik.userservice.security.MyUserDetails
import com.yurasik.userservice.security.SecurityConstants
import io.jsonwebtoken.ExpiredJwtException
import io.jsonwebtoken.JwtException
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Component
import java.io.IOException

import javax.annotation.PostConstruct
import javax.servlet.http.HttpServletRequest
import javax.servlet.ServletException
import org.springframework.security.core.userdetails.UserDetails
import java.io.File
import java.io.FileWriter
import java.nio.file.Files
import java.security.*
import java.security.spec.PKCS8EncodedKeySpec
import java.security.spec.X509EncodedKeySpec
import java.security.PrivateKey
import java.nio.file.Paths
import java.util.*


@Component
class JwtTokenProvider {

    /**
     * THIS IS NOT A SECURE PRACTICE! For simplicity, we are storing a static key here. Ideally, in a
     * microservices environment, this key would be kept on a config-server.
     */
    @Value("\${security.jwt.token.secret-key}")
    private var privateKeySecurity: String? = SecurityConstants.JWT_SECRET

    @Value("\${security.jwt.token.refresh-secret-key}")
    private var publicKeySecutiry: String? = SecurityConstants.JWT_SECRET

    @Value("\${security.jwt.token.expire-length}")
    private val validityInMilliseconds: Long = 1000 // 1h

    // Keys
    private lateinit var authPublicKey: PublicKey
    private lateinit var authPrivateKey: PrivateKey

    private lateinit var refreshPublicKey: PublicKey
    private lateinit var refreshPrivateKey: PrivateKey

    @Autowired
    private lateinit var myUserDetails: MyUserDetails

    companion object {
        val log = LoggerFactory.getLogger(JwtTokenProvider::class.java)
    }

    @PostConstruct
    protected fun init() {
        authPrivateKey = readPrivateKeyBase64("authKeyBase64") //loadPrivateKey()
        authPublicKey = readPublicKey64("authKeyBase64") //loadPublicKey()
        refreshPrivateKey = readPrivateKeyBase64("refreshKeyBase64")
        refreshPublicKey = readPublicKey64("refreshKeyBase64")

        //generateKey()
        //saveKeysToBaseB4(privateKey, publicKey, "publicKeyBase64")
    }

    private fun readPrivateKeyBase64(fileName: String): PrivateKey {
        val decoder = Base64.getMimeDecoder()
        val file = File("$fileName.key")
        val src =file.readText()
            .replace("-----BEGIN RSA PRIVATE KEY-----\n","")
            .replace("\n-----END RSA PRIVATE KEY-----\n", "")
            .trim()

        val ks = PKCS8EncodedKeySpec(decoder.decode(src))
        val kf = KeyFactory.getInstance("RSA")
        val pvt = kf.generatePrivate(ks)
        return pvt
    }

    private fun readPublicKey64(fileName: String): PublicKey {
        /* Read all bytes from the private key file */
        val decoder = Base64.getMimeDecoder()
        val file = File("$fileName.pub")
        val src =file.readText()
            .replace("-----BEGIN RSA PUBLIC KEY-----\n","")
            .replace("\n-----END RSA PUBLIC KEY-----\n", "")
            .trim()

        /* Generate public key. */
        val ks = X509EncodedKeySpec(decoder.decode(src))
        val kf = KeyFactory.getInstance("RSA")
        val pub = kf.generatePublic(ks);
        return pub
    }

    private fun saveKeysToBaseB4(privateKey: PrivateKey, publicKey: PublicKey, fileName: String) {
        val encoder = Base64.getMimeEncoder()

        var outw = FileWriter("$fileName.key")
        outw.write("-----BEGIN RSA PRIVATE KEY-----\n")
        outw.write(encoder.encodeToString(privateKey.encoded))
        outw.write("\n-----END RSA PRIVATE KEY-----\n")
        outw.close()

        outw = FileWriter("$fileName.pub")
        outw.write("-----BEGIN RSA PUBLIC KEY-----\n")
        outw.write(encoder.encodeToString(publicKey.encoded))
        outw.write("\n-----END RSA PUBLIC KEY-----\n")
        outw.close()
    }

    private fun loadPrivateKey(): PrivateKey {
        /* Read all bytes from the private key file */
        val path = Paths.get("authKey.key")
        val bytes = Files.readAllBytes(path)

        /* Generate private key. */
        val ks = PKCS8EncodedKeySpec(bytes)
        val kf = KeyFactory.getInstance("RSA")
        val pvt = kf.generatePrivate(ks)
        return pvt
    }

    private fun loadPublicKey(): PublicKey {
        /* Read all bytes from the private key file */
        val path = Paths.get("authKey.pub")
        val bytes = Files.readAllBytes(path)

        /* Generate public key. */
        val ks = X509EncodedKeySpec(bytes)
        val kf = KeyFactory.getInstance("RSA")
        val pub = kf.generatePublic(ks);
        return pub
    }

    private fun generateKey() {
        val  kpg = KeyPairGenerator.getInstance("RSA")
        kpg.initialize(2048)
        val kp = kpg.genKeyPair()
        val pub = kp.public
        val pvt = kp.private

        val kf = KeyFactory.getInstance("RSA")
        val keySpecPKCS8 = PKCS8EncodedKeySpec(pvt.encoded)
        authPrivateKey = kf.generatePrivate(keySpecPKCS8)

        val keySpecX509 = X509EncodedKeySpec(pub.encoded)
        authPublicKey = kf.generatePublic(keySpecX509)
    }

    fun createAccessJwtToken(username: String): String {
        val user = myUserDetails.loadUserByUsername(username)
        val token = createAccessJwtToken(user)
        log.info("Create token: $token")
        return token
    }

    fun createRefreshToken(username: String): String {
        val now = Date()
        val validity = Date(now.time + validityInMilliseconds * 2)

        return Jwts.builder()
            .signWith(refreshPrivateKey, SignatureAlgorithm.RS512)
            .setHeaderParam(SecurityConstants.TYPE, SecurityConstants.TOKEN_TYPE)
            .setIssuer(SecurityConstants.TOKEN_ISSUER)
            .setAudience(SecurityConstants.TOKEN_AUDIENCE)
            .setSubject(username)
            .setIssuedAt(now)
            .setExpiration(validity)
            .compact()
    }

    fun createAccessJwtToken(userDetails: UserDetails): String {
        log.info("Validity time: $validityInMilliseconds")

        val now = Date()
        val validity = Date(now.time + validityInMilliseconds)

        return Jwts.builder()
            .signWith(authPrivateKey, SignatureAlgorithm.RS512)
            .setHeaderParam(SecurityConstants.TYPE, SecurityConstants.TOKEN_TYPE)
            .setIssuer(SecurityConstants.TOKEN_ISSUER)
            .setAudience(SecurityConstants.TOKEN_AUDIENCE)
            .setSubject(userDetails.username)
            .setIssuedAt(now)
            .setExpiration(validity)
            .claim(SecurityConstants.ROLE, userDetails.authorities)
            .compact()
    }

    fun getAuthentication(token: String): Authentication {
        val userDetails = myUserDetails.loadUserByUsername(getUsername(token))
        return UsernamePasswordAuthenticationToken(userDetails, "", userDetails.authorities)
    }

    fun getUsername(token: String): String {
        return Jwts.parser().setSigningKey(authPublicKey).parseClaimsJws(token).body.subject
    }

    fun resolveToken(req: HttpServletRequest): String? {
        val bearerToken = req.getHeader("Authorization")
        return if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
            //log.info("Clazz: ${}")
            val token = bearerToken.replace("Bearer", "").trim()
            log.info("Token : $token")
            token.toString()
        } else null
    }

    @Throws(ServletException::class, IOException::class, CustomException::class, SignatureException::class)
    fun validateToken(token: String): Boolean {
        log.info("validateToken: $token")
        //try {
        //    Jwts.parser().setSigningKey(authPublicKey).parseClaimsJws(token)
        //} catch (e: Exception) {
        //    e.printStackTrace()
        //}
        //return true
        try {
            //log.info("Secret key: $secretKey")
            //Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token)
            Jwts.parser().setSigningKey(authPublicKey).parseClaimsJws(token)
            return true
        } catch (e: ExpiredJwtException) {
            e.printStackTrace()
            throw CustomException(e.message ?: "Expired or invalid JWT token",
                HttpStatus.FORBIDDEN,
                parentException = e)
        } catch (e: JwtException) {
            e.printStackTrace()
            throw CustomException(e.message ?: "Expired or invalid JWT token",
                HttpStatus.INTERNAL_SERVER_ERROR,
                parentException = e)
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
            throw CustomException(e.message ?: "Expired or invalid JWT token",
                HttpStatus.INTERNAL_SERVER_ERROR,
                parentException = e)
        } catch (e: SignatureException) {
            e.printStackTrace()
            throw CustomException(e.message ?: "Expired or invalid JWT token",
                HttpStatus.INTERNAL_SERVER_ERROR,
                parentException = e)
        }

    }

}
