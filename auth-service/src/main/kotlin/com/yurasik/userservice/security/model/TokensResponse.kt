package com.yurasik.userservice.security.model

import java.util.*

data class TokensResponse(val timestamp: Date? = Date(),
                          val status: Int,
                          val token: String,
                          val refreshToken: String? = null,
                          val message: String,
                          val path: String? = "")
