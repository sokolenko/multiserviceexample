package com.yurasik.userservice.security.jwt

import com.fasterxml.jackson.databind.ObjectMapper
import com.yurasik.userservice.exception.CustomException
import com.yurasik.userservice.exception.ErrorsDetails
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.security.core.AuthenticationException
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.AuthenticationEntryPoint
import org.springframework.stereotype.Component
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class JwtAuthenticationEntryPoint : AuthenticationEntryPoint  {

    companion object {
        val log = LoggerFactory.getLogger(JwtAuthenticationEntryPoint::class.java)
    }

    @Autowired
    private lateinit var mapper: ObjectMapper

    override fun commence(
        request: HttpServletRequest?,
        response: HttpServletResponse?,
        authException: AuthenticationException?
    ) {
        // This is invoked when user tries to access a secured REST resource without supplying any credentials
        // We should just send a 401 Unauthorized response because there is no 'login page' to redirect to
        // Here you can place any message you want
        log.info("commence: $authException")
        //response?.sendError(HttpServletResponse.SC_UNAUTHORIZED, authException?.message);
        placeErrorMessage(CustomException(parentException = authException, httpStatus = HttpStatus.FORBIDDEN),
            response!!, request!!)
    }

    private fun placeErrorMessage(
        ex: CustomException,
        httpServletResponse: HttpServletResponse,
        httpServletRequest: HttpServletRequest
    ) {
        log.info("Exception: ${ex.parentException}")
        SecurityContextHolder.clearContext()
        // notify client of response body content type
        httpServletResponse.addHeader("Content-Type", "application/json;charset=UTF-8");
        httpServletResponse.status = HttpServletResponse.SC_UNAUTHORIZED
        val detail = ErrorsDetails(
            timestamp = Date(),
            status = HttpServletResponse.SC_UNAUTHORIZED,
            error = ex.parentException?.javaClass?.simpleName ?: ex.message,
            message = ex.parentException?.message ?: ex.message,
            path = httpServletRequest.requestURL.toString() + "/" + (httpServletRequest.queryString ?: "")
        )
        mapper.writeValue(httpServletResponse.outputStream, detail)
        httpServletResponse.flushBuffer()
    }
}