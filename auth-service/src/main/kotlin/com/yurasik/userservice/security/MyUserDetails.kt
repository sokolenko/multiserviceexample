package com.yurasik.userservice.security

import com.yurasik.userservice.repository.UserRepository
import com.yurasik.userservice.security.jwt.JwtAuthenticationFilter
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

@Service
class MyUserDetails : UserDetailsService {

    @Autowired
    private lateinit var userRepository: UserRepository

    companion object {
        val loggger = LoggerFactory.getLogger(MyUserDetails::class.java)
    }

    @Throws(UsernameNotFoundException::class)
    override fun loadUserByUsername(username: String): UserDetails {
        val (_, _, password, _, _, _, _, roles) = userRepository.findByUsername(username) ?: throw UsernameNotFoundException("User '$username' not found")

        loggger.info("roles: $roles, password: $password")
        return org.springframework.security.core.userdetails.User//
            .withUsername(username)//
            .password(password!!)//
            .authorities(roles!!)//
            .accountExpired(false)//
            .accountLocked(false)//
            .credentialsExpired(false)//
            .disabled(false)//
            .build()
    }

}
