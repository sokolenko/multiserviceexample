package com.yurasik.userservice.security.jwt.processing

import org.springframework.security.web.util.matcher.AntPathRequestMatcher
import org.springframework.security.web.util.matcher.RequestMatcher
import org.springframework.util.Assert
import javax.servlet.http.HttpServletRequest
import org.springframework.security.web.util.matcher.OrRequestMatcher



class SkipPathRequestMatcher(pathsToSkip: List<String>, processingPath: String) : RequestMatcher {

    private val matchers: OrRequestMatcher
    private val processingMatcher: RequestMatcher

    init {
        val requestMatches = pathsToSkip.map { AntPathRequestMatcher(it) }.toList()
        matchers = OrRequestMatcher(requestMatches)
        processingMatcher = AntPathRequestMatcher(processingPath)
    }

    override fun matches(request: HttpServletRequest?) = when {
        matchers.matches(request) -> false
        else -> processingMatcher.matches(request)
    }
}