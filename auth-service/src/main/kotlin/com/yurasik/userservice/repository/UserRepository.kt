package com.yurasik.userservice.repository

import com.yurasik.userservice.security.model.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

import javax.transaction.Transactional

@Repository
interface UserRepository : JpaRepository<User, Int> {

    fun existsByUsername(username: String): Boolean

    fun findByUsername(username: String): User?

    @Transactional
    fun deleteByUsername(username: String)

}
