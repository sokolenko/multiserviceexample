package com.yurasik.userservice

import com.fasterxml.jackson.databind.ObjectMapper
import com.yurasik.userservice.security.model.RoleEnum
import com.yurasik.userservice.security.model.User
import com.yurasik.userservice.security.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.web.servlet.config.annotation.EnableWebMvc

@SpringBootApplication
class UserServiceApplication : CommandLineRunner {

	@Autowired
	lateinit var userService: UserService

	@Bean
	fun objectMapper() = ObjectMapper()

	override fun run(vararg args: String?) {
		val admin = User(username = "admin", password = "password",
			roles = mutableSetOf(RoleEnum.ADMIN), email = "admin@email.com")
		userService.signup(admin)

		val client = User(username = "user", password = "password",
			roles = mutableSetOf(RoleEnum.USER), email = "usern@email.com")
		userService.signup(client)
	}

}

fun main(args: Array<String>) {
	runApplication<UserServiceApplication>(*args)
}
