
plugins {
	kotlin("jvm")
}

dependencies {
	(kotlin("stdlib-jdk8"))
	compile("com.fasterxml.jackson.module:jackson-module-kotlin")

	compile("org.springframework.boot:spring-boot-starter-web")
	compile("org.springframework.boot:spring-boot-autoconfigure")
	compile("org.springframework.boot:spring-boot-starter-parent:2.1.9.RELEASE")
	compile("org.springframework.boot:spring-boot-starter-security")
	compile("org.springframework.boot:spring-boot-starter-data-jpa")
	compile("org.springframework.boot:spring-boot-starter-actuator")
	compile("org.springframework.boot:spring-spring-security-jwt")

	compile("org.springframework.boot:spring-boot-devtools")

	compile("com.j2html:j2html:1.3.0")

	compile("io.github.microutils:kotlin-logging:1.6.22")

	// https://mvnrepository.com/artifact/io.jsonwebtoken/jjwt
	compile ("io.jsonwebtoken:jjwt-api:0.10.7")
	compile ("io.jsonwebtoken:jjwt-impl:0.10.7")
	compile ("io.jsonwebtoken:jjwt-jackson:0.10.7")
	compile("com.auth0:java-jwt:3.8.3")

	compile("com.h2database:h2:1.4.199")

	testImplementation("org.springframework.boot:spring-boot-starter-test")
	testImplementation("org.junit.jupiter:junit-jupiter-api")
	testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
}